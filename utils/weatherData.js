var constants = require('../config')

var request = require('request')

var weatherData = (address, callback) => {
  const url =
    constants.openWeatherData.BASE_URL +
    encodeURIComponent(address) +
    '&appid=' +
    constants.openWeatherData.SECRET_KEY
  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback(undefined)
    } else {
      callback(undefined, {
        temperature: body.main.temp,
        description: body.weather[0].description,
        cityName: body.name,
      })
    }
  })
}

module.exports = weatherData
